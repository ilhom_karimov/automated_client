import sys
sys.path.append('..')


from yaml import load, dump

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class Config(object):

    def __init__(self):
        self.stream = open('config/config.yml', 'r')
        self.config = load(self.stream.read(), Loader=Loader)

    def get_config(self):
        return self.config
import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog import events
from io import BytesIO
from PIL import Image
import os
import requests
import json
from config import Config

import boto3

class myHandler(events.FileSystemEventHandler):
    def __init__(self, photographer_id, collection_id, event_id, price, upload_url, index_url):
        self.patterns = ["*.jpg", "*.jpeg", "*.png", "*.bmp", "*.pdf", "*.txt"]
        self.images = (".jpg", ".jpeg", ".png", ".bmp")
        self.collection_id = collection_id
        self.photographer_id = photographer_id
        self.event_id = event_id
        self.price = price
        self.upload_url = upload_url
        self.index_url = index_url

    def process(self, event):
        """
        event.event_type
            'modified' | 'created' | 'moved' | 'deleted'
        event.is_directory
            True | False
        event.src_path
            path/to/observed/file
        """
        # the file will be processed there
        print event.src_path, event.event_type  # print now only for debug

    def on_modified(self, event):
        self.process(event)

    def on_created(self, event):
        self.process(event)
        #s3 = boto3.resource('s3')

        # Get new file name
        name = str(event.src_path)
        print name
        folder, filename = os.path.split(name)
        print filename
        key = filename
        if not key.lower().endswith(self.images):
            return       
		
		
        # Define width
        #size = 512
        #basewidth = size
        
        image_type = 'JPEG'        
        img = Image.open(name)
        (width, height) = img.size
        filesize = os.stat(name).st_size
        data = {
			"event_id": self.event_id,
			"photographer_id": self.photographer_id,
			"collection_id": self.collection_id,
			"price": self.price,
			"filesize": filesize,
			"width": width,
			"height": height
		}
        
        result = requests.post(self.upload_url, json.dumps(data))
        
        print result.text
        result = json.loads(result.text)
        upload_url = result['data']['upload_url']
        photo_id = result['data']['photo_id']
        print upload_url		
        #wpercent = (basewidth / float(img.size[0]))
        #hsize = int((float(img.size[1]) * float(wpercent)))
        #img.thumbnail([basewidth, hsize])
        output = BytesIO()
        img.save(output, image_type)
        output.seek(0)
        
        upload_result = requests.put(upload_url, output)
        print upload_result

        # Call index
        index_data = {
            'collection_id': self.collection_id,
            'photo_id': photo_id
        }

        index_response = requests.post(self.index_url, json.dumps(index_data))
        print index_response.text

        #bucket = 'testbucketfr1'

        #s3.Bucket(bucket).put_object(Key=name2[1], Body=output)
        

if __name__ == "__main__":
    # Configure loging
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    # Set path
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    print path

    uploader_config = Config()

	# Setting required variables
    photographer_id = uploader_config.get_config()['photographer']['id']['value']


    # Searching for events
    search_query = raw_input('please, enter event name: ')
    search_data = {
        'photographer_id': photographer_id,
        'event_name': search_query
    }
    event_details_url = uploader_config.get_config()['endpoints']['event_details']['url']
    event_results = requests.post(event_details_url, json.dumps(search_data))
    events = json.loads(event_results.text)
    
    if not events['data']:
        print 'Events with this name were not found. Please try again'
        sys.exit()
    

    # Displaying found events to the user
    events = events['data']['events']
    
    events_dict = {}
    for event in events:
        events_dict[event['id']] = event
        print type(event['date'])

    print '\n'*100

    print 'Following events were found. Please choose the event by entering its id:'
    for event in events_dict:
        print 'id: {}\nname: {}\nplace: {}\ndate: {}\n\n'.format(
            event, 
            events_dict[event]['event_name'], 
            events_dict[event]['place_name'],
            events_dict[event]['date'])
    event_id = int(raw_input('Event id: ').strip())
    while event_id not in events_dict:
        print 'Please enter the correct id next time'
        event_id = int(raw_input('Event id: ').strip())

    print '\n'*100


    # Starting the observer
    print 'Chosen event:\nid: {}\nname: {}\nplace: {}\ndate: {}\n\n'.format(
            event, 
            events_dict[event_id]['event_name'], 
            events_dict[event_id]['place_name'],
            events_dict[event_id]['date'])
    print 'Waiting for new photos...'

    collection_id = events_dict[event_id]['collection_id']
    price = events_dict[event_id]['price']

    upload_url = uploader_config.get_config()['endpoints']['upload']['url']
    index_url = uploader_config.get_config()['endpoints']['index']['url']

    # Setting eventHandler variable to LoggingEventHandler() function
    event_handler = myHandler(photographer_id, collection_id, event_id, price, upload_url, index_url)

    # Setting observer variable to Observer
    observer = Observer()

    #Scheduling to observer the loggingEventHandler
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    except Exception, e:
        print e
    observer.join()
    
